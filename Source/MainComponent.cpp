/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"

//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);

	// 2 spaces between "Impulses"
	audioDeviceManager.setMidiInputEnabled("Novation Impulse", true);
	audioDeviceManager.addMidiInputCallback(String::empty, this);

	audioDeviceManager.addAudioCallback(this);

	// midiLabel
	addAndMakeVisible(midiLabel);
	midiLabel.setText("Default", dontSendNotification);
}

MainComponent::~MainComponent()
{
	audioDeviceManager.removeMidiInputCallback(String::empty, this);
	audioDeviceManager.removeAudioCallback(this);
}

void MainComponent::resized()
{
	midiLabel.setBounds(getLocalBounds().reduced(100));
}

void MainComponent::handleIncomingMidiMessage(MidiInput* source, const MidiMessage& message)
{
	String midiText;

	if (message.isNoteOnOrOff())
	{
		midiText.clear();
		midiText << "NoteOn: Channel " << message.getChannel();
		midiText << ":Number" << message.getNoteNumber();
		midiText << ":Velocity" << (String)message.getVelocity();
	}
	else if (message.isController())
	{
		midiText.clear();
		midiText << "Controller: Number " << message.getControllerNumber();
		midiText << ":Value" << message.getControllerValue();
	}

	midiLabel.getTextValue() = midiText;
}

void MainComponent::audioDeviceIOCallback(const float** inputChannelData, int numInputSamples, float** outputChannelData, int numOutputSamples, int numSamples)
{
	const float *inL = inputChannelData[0];
	const float *inR = inputChannelData[1];
	float *outL = outputChannelData[0];
	float *outR = outputChannelData[1];

	while (numSamples--)
	{
		*outL = *inL;
		*outR = *inR;

		inL++;
		inR++;
		outL++;
		outR++;
	}
}

void MainComponent::audioDeviceAboutToStart(AudioIODevice* device)
{

}

void MainComponent::audioDeviceStopped()
{

}