/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"


//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component,
						public MidiInputCallback,
						public AudioIODeviceCallback
{
public:
    //==============================================================================
    MainComponent();
    ~MainComponent();

    void resized() override;

	void handleIncomingMidiMessage(MidiInput* source, const MidiMessage& message) override;

	void audioDeviceIOCallback(const float** inputChannelData,
								int numInputSamples,
								float** outputChannelData,
								int numOutputSamples,
								int numSamples) override;

	void audioDeviceAboutToStart(AudioIODevice* device) override;

	void audioDeviceStopped() override;

private:
	AudioDeviceManager audioDeviceManager;
	Label midiLabel;
	Slider slider;

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
